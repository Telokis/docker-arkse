#!/bin/bash

function stop {
    echo "Server stopped. Performing graceful stop"
    arkmanager stop --saveworld --warn --verbose

    if [[ $BACKUPONSTOP == "true" ]]; then
        echo "Backing up save"
        arkmanager backup --verbose
    fi

	exit
}

echo "Setting permissions for /home/steam/ARK/ShooterGame/Saved"
chown -R steam:steam "/home/steam/ARK/ShooterGame/Saved"

declare -A OTHER_VARIABLES=( 
    [serverMap]=1
    [serverMapModId]=1
)

function generate_config {
    echo "###################################################################"
    echo "# This file is auto generated. It will be overriden at next launch."
    echo ""
    echo "arkserverroot=/home/steam/ARK"

    while IFS='=' read -r -d '' n v; do
        if  [[ $n == ark_* ]] || [[ $n == arkopt_* ]] || [[ -n "${OTHER_VARIABLES[$n]}" ]] ; then
            echo "$n=\"$v\""
        elif [[ $n == arkflag_* ]] && [[ $v != "false" ]] ; then
            echo "$n=\"$v\""
        fi
    done < <(env -0)
    
    echo "###################################################################"
}

function generate_game_ini {
    echo ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;"
    echo "; This file is auto generated. It will be overriden at next launch."
    echo ""
    echo "[/script/shootergame.shootergamemode]"

    while IFS='=' read -r -d '' n v; do
        if  [[ $n == arkgame_* ]] ; then
            echo "${n#"arkgame_"}=$v"
        fi
    done < <(env -0)
    
    echo ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;"
}

generate_config > /etc/arkmanager/instances/main.cfg
echo "Generated config:"
cat /etc/arkmanager/instances/main.cfg

if [[ ${ark_GameModIds+x} ]]; then
    echo "Installing mods"
    arkmanager installmods --verbose
else
    echo "No mod to install"
fi

if [[ $UPDATEONSTART == "true" ]]; then
    echo "Updating instance"
	arkmanager update --verbose
fi

generate_game_ini > /home/steam/ARK/ShooterGame/Saved/Config/LinuxServer/Game.ini
echo "Generated Game.ini:"
cat /home/steam/ARK/ShooterGame/Saved/Config/LinuxServer/Game.ini

# Stop server in case of signal INT or TERM
echo "Waiting..."
trap "stop" INT
trap "stop" TERM
trap "stop" SIGINT
trap "stop" SIGTERM

arkmanager run --verbose &

wait $!

stop