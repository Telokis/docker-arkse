FROM debian:9.8-slim

MAINTAINER Telokis

ENV STEAMCMDDIR /home/steam/steamcmd

RUN set -x \
	&& apt-get update \
	&& apt-get install -y --no-install-recommends --no-install-suggests \
		lib32stdc++6 \
		lib32gcc1 \
		curl \
		ca-certificates \
		perl-modules \
		lsof \
		libc6-i386 \
		bzip2 \
		libcompress-raw-zlib-perl \
	&& useradd -m steam \
	&& su steam -c \
		"mkdir -p ${STEAMCMDDIR} \
		&& cd ${STEAMCMDDIR} \
		&& curl 'https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz' | tar -vxz" \
	&& apt-get clean autoclean \
	&& apt-get autoremove -y

RUN curl -sL http://git.io/vtf5N | bash -s steam

RUN arkmanager install --verbose || true

WORKDIR /home/steam
VOLUME /home/steam/ARK/ShooterGame/Saved
VOLUME /home/steam/ARK-Backups

COPY global.cfg /etc/arkmanager/arkmanager.cfg
COPY start.sh /home/steam/start.sh

EXPOSE 7778:7778/udp 7778:7778 27015:27015/udp 27015:27015 32330:32330

ENV serverMap=TheIsland \
	ark_Port=7778 \
	ark_QueryPort=27015 \
	ark_RCONPort=32330 \
	ark_SessionName=ArkSE \
	ark_MaxPlayers=10 \
	arkflag_NoBattlEye=true \
	arkflag_log=true \
	arkflag_NoTransferFromFiltering=true \
	UPDATEONSTART=true \
	BACKUPONSTOP=true

ENTRYPOINT ["/home/steam/start.sh"]
