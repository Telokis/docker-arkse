# docker-arkse

Docker image for Ark Survival Evolved based on [ark-server-tools](https://github.com/FezVrasta/ark-server-tools).

This image is a thin wrapper around ark-server-tools. It basically installs everything needed and dynamically generates a config file based on the environment variables.

You can find prebuilt docker images [on the docker hub](https://hub.docker.com/r/telokis/arkse).

## Configuring the server

Every Ark option can be configured through environment variables. There is no need for config files.

### Complete game options

You can take a look at [this section](https://github.com/FezVrasta/ark-server-tools#instance-config-files) to see what environment variables can be used.  
Basically, any option taken from [the official configuration](https://ark.gamepedia.com/Server_Configuration#GameUserSettings.ini) can be prefixed with `ark_` to assign a value to it.

For example, if I want to change the port, I can do:

```
ark_Port=7779
```

### Game.ini configuration

Any Game.ini option taken from [the official configuration](https://ark.gamepedia.com/Server_Configuration#Game.ini) can be prefixed with `arkgame_` to assign a value to it.  
The container will dynamically generate the file at each startup, allowing you to change options on the fly.

For example, if I want to change the egg hatch speed multiplier, I can do:

```
arkgame_EggHatchSpeedMultiplier=2.0
```

### Server map

Aside from these options, there are ones not following this pattern. The game map is one of them. Specifying it is done using the `serverMap` variable.

```
serverMap=TheCenter
```

### Image-related options

There are options used to specify the way the container behaves:

- `UPDATEONSTART`: if set to `"true"`, tries to update the server when starting the container. Meaning it only takes a restart to update the game.
- `BACKUPONSTOP`: if set to `"true"`, will automatically backup the save when the server stops.

## Ports needed

The game port (specified with `ark_Port`, defaults to `7778`) and the QueryPort (specified with `ark_QueryPort`, defaults to `27015`) must be linked through both TCP and UDP in order for them to work properly.

Their default values are as such:

```
ark_Port: 7778
ark_QueryPort: 27015
```

which, in turn, leads to the following port links in the image:

```
ports:
    - 7778:7778/udp
    - 7778:7778
    - 27015:27015/udp
    - 27015:27015
```

If you use the RCONPort (Specified with `ark_RCONPort`, defaults to `32330`), it only needs to be linked through TCP.

## Important directories

The save data will be stored inside `/home/steam/ARK/ShooterGame/Saved`.  
The backups will be stored inside `/home/steam/ARK-Backups`.

Those directories may be used with volumes in order to preserve their contents.

## Default configuration

The image configures the following variables by default:

```
serverMap=TheIsland

ark_Port=7778
ark_QueryPort=27015
ark_RCONPort=32330
ark_SessionName=ArkSE
ark_MaxPlayers=10

arkflag_NoBattlEye=true
arkflag_log=true
arkflag_NoTransferFromFiltering=true

UPDATEONSTART=true
```

## Example docker-compose.yml

```yml
arkse:
  container_name: arkse
  image: telokis/arkse
  environment:
    serverMap: TheIsland
    ark_Port: 7778
    ark_QueryPort: 27015
    ark_RCONPort: 32330
    ark_SessionName: ArkSE
    ark_MaxPlayers: 10
    ark_ServerPassword: "mypassword"
    ark_GameModIds: 731604991,719928795,779897534,722649005,693416678,630601751,519998112
    arkflag_NoBattlEye: "true"
    arkflag_log: "true"
    arkflag_NoTransferFromFiltering: "true"
    UPDATEONSTART: "true"
  volumes:
    - ./save:/home/steam/ARK/ShooterGame/Saved
    - ./backups:/home/steam/ARK-Backups
  ports:
    - 7778:7778/udp
    - 7778:7778
    - 27015:27015/udp
    - 27015:27015
    - 32330:32330
```
